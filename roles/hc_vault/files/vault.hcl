ui = true

storage "file" {
  path = "/opt/vault"
}

listener "tcp" {
  address = "0.0.0.0:8200"
  tls_disable = true
}
