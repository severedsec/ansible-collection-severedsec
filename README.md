# Infrastructure collection

A set of ansible roles for provisioning base level servers including a
hashistack. The baseline is build to join servers to a tailscale overlay network
so that all internal communication and admin portal access occurs over encrypted
connections. For end user access control it is assumed that you will utilize
some sort of identity aware proxy such as Pomerium or Cloudflare Access.


## Currently working roles

- Baseline: makes some changes to the host operating system including setting
  hostname based on inventory hostname and joining to a tailscale tailnet with
  an authkey. variable tailscale_authkey must be set and should be stored in a
  secrets manager.
- hashicorp vault: deploys a single instance of hashicorp vault to a server.
  - future addition: we will be adding configuration to point vault to an oidc
    provider once the keycloak role is complete.

## in progress

- keycloak: setting up a cloud agnostic identity provider.
- hashicorp nomad: create a cluster of nomad servers and clients based on groups
  in the inventory
- hashicorp consul: create a cluster of consul servers and clients based on groups
  in the inventory. generally deployed on the same servers as nomad.
- minio: create a cloud agnostic S3 storage provider. We intend for this to be
  accessible only by the container servers to provide S3 storage for container
  workloads.
- Postgres: This role is intended to provide a database for environments where
  managed database is not available. If you are using a provider which offers
  managed databases, use that instead.
